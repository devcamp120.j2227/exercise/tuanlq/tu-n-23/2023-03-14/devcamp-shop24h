import { 
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem,
    NavLink,
    Container,
} from 'reactstrap';
import logo from "../assets/images/logo.png";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {  faShoppingCart } from '@fortawesome/free-solid-svg-icons';
import { faFacebook, faTwitter, faInstagram } from '@fortawesome/free-brands-svg-icons';
import { useState } from 'react';
import ShopBreadcrump from './AppBreadcrumb';

export default function Navigator () {
    //navbar
    const [isOpen, setIsOpen] = useState(false);
    const toggle = () => setIsOpen(!isOpen);
    return(
        <>
            <Container>
                <Navbar  fixed="top" expand='md'  dark= {true}  className="container" id='nav1'>
                    <NavbarBrand href="/">
                        <img src={logo} alt="logo" className='logo'/>
                    </NavbarBrand>
                    <NavbarToggler onClick={toggle} />
                    <Collapse isOpen={isOpen}  navbar  >
                        <Nav  navbar  className='w-100 hearder-link-position' horizontal='end'>
                            <NavItem>
                                <NavLink href="/" className='headerLink mx-3'><span>Men</span></NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink href="/" className='headerLink mx-3'><span>Woman</span></NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink href="/" className='headerLink mx-3'><span>Child</span></NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink href="/"  className='headerLink mx-3'><span>Collection</span></NavLink>
                            </NavItem>
                        </Nav>
                        <Nav  navbar   className='w-50 ' horizontal='end'>
                            <NavItem>
                                <NavLink href="/"><span className='mx-3'><FontAwesomeIcon icon={faFacebook} size="lg"/></span></NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink href="/"><span className='mx-3'><FontAwesomeIcon icon={faInstagram} size="lg"/></span></NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink href="/"><span className='mx-3'><FontAwesomeIcon icon={faTwitter} size="lg"/></span></NavLink>
                            </NavItem>
                            <NavItem>
                                <NavLink href="/"><span className='mx-3'><FontAwesomeIcon icon={faShoppingCart} size="lg"/></span></NavLink>
                            </NavItem>
                        </Nav>
                    </Collapse>
                </Navbar>
            </Container>
            <ShopBreadcrump></ShopBreadcrump>
        </>
    )
}