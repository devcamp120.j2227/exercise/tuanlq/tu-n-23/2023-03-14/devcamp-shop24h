export const SEND_REQUEST_TAKE_PRODUCT_PENDING = "Bắt đầu lấy dữ liệu product từ database";

export const SEND_REQUEST_TAKE_PRODUCT_START = "Lấy dữ liệu product thành công";

export const SEND_REQUEST_TAKE_PRODUCT_END = "Lấy dữ liệu product thất bại";