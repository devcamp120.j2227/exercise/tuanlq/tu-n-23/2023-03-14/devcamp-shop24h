import { 
    SEND_REQUEST_TAKE_PRODUCT_PENDING,
    SEND_REQUEST_TAKE_PRODUCT_START,
    SEND_REQUEST_TAKE_PRODUCT_END
 } from "../constant/constant";
const initialState = {
    products: []
};

const productReducer = (state = initialState, action) => {
    switch (action.type){
        case SEND_REQUEST_TAKE_PRODUCT_PENDING:
            break;
        case SEND_REQUEST_TAKE_PRODUCT_START:
            state.products = action.data.data;
            console.log(state.products);
            break;
        case SEND_REQUEST_TAKE_PRODUCT_END:
            break;
        default:
            break;

    }
    return{...state};
}

export default productReducer