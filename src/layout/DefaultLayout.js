import {Container} from 'reactstrap';
import Footer from '../components/AppFooter';
import Navigator from '../components/Navigator';
import Content from '../components/AppContent';
function DefaultLayout() {
    
    return( 
        <div>
            <Container  className='bg-img'>
                {/* Navbar */}
                 <Navigator></Navigator>
                {/*Content*/}
                <Content/>
                {/* footer */}
                <Footer></Footer>
            </Container>
        </div>
    )
}

export default DefaultLayout