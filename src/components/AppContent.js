import { Container } from "reactstrap";
import {Navigate, Route, Routes} from "react-router-dom";

//routes config
import RouteList from "../Routes";
export default function AppContent () {
    console.log(RouteList)
    return(
        <>
            <Routes>
                {RouteList.map((route, index) => {
                    return(
                        route.element && (
                            <Route
                                key={index}
                                path={route.path}
                                exact ={route.exact}
                                name={route.name}
                                element = {<route.element/>}
                            />
                        )
                    )
                })}
                 <Route path="/" element={<Navigate to="Home Page" replace />} />
            </Routes>
        </>
    )
}