import HomePage from "./views/Home/HomePage"

import React from "react"

const Home = React.lazy(() => import('./views/Home/HomePage'));
const Products = React.lazy(() => import("./views/ProductsPages"));
const RouteList = [
    {path: "/", exact: true ,name: "HomePage", element: Home},
    {path:"/Products", exact: true, name: "Products", element:Products}
]
export default RouteList