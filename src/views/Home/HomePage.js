import Product from "./Product"
import { useEffect, useRef, useState } from 'react';
import {Container, Row, Col, UncontrolledCarousel,Button} from 'reactstrap';
export default function HomePage(){
     //state for countdown clock
     const [timeDays, setTimerDays] = useState("00");
     const [timeHours, setTimerHours] = useState("00");
     const [timeMinutes, setTimerMinutes] = useState("00");
     const [timeSeconds, setTimerSeconds] = useState("00"); 
     
     let interval =  useRef();
     // Function set countdown for the deal
     const startTimer = () => {
         const countDownDate = new Date("December 30 2022 00:00:00").getTime();
 
         interval = setInterval(()=> {
             const now = new Date().getTime();
             const distance = countDownDate - now;
             const days = Math.floor(distance / (1000 * 60 * 60 * 24));
             const hours = Math.floor(distance % (1000 * 60 * 60 * 24) / (1000 * 60 * 60));
             const minutes = Math.floor(distance % (1000 * 60 * 60 ) / (1000 * 60));
             const seconds = Math.floor(distance % (1000 * 60  ) / 1000 );
 
             if(distance < 0 ){
                 //stop our time
                 clearInterval(interval.current)
             } else {
                 //update time
                 setTimerDays(days);
                 setTimerHours(hours);
                 setTimerMinutes(minutes);
                 setTimerSeconds(seconds);
             }
         }, 1000);
     }
     
     
     useEffect(()=>{
         startTimer();
         
     })
    return(
        <>
           {/* Carousel */}
           <div className='carousel'>
                    <UncontrolledCarousel  interval={false}
                        items={[
                            {
                            altText: '', 
                            key: 1,
                            src: require("../../assets/images/carousel/1.jpg"),
                            caption:""
                            },
                            {
                            altText: '',
                            key: 2,
                            src: require("../../assets/images/carousel/2.jpg"),
                            caption:""
                            },              
                        ]}
                        />
                </div>
                {/* Collection */}
                <div className='Collection'>
                    <Container>
                        <Row >
                            <Col lg={12} >
                                <div className='banner__item d-flex justify-content-center'>
                                    <img src={require("../../assets/images/collection2022/nmd.webp")} className="collection-images" alt="nmd" />
                                    <div className='banner-collection-nmd'>
                                        <h2 className='mx-3 mt-2 mb-4' >Nmd Collection 2022</h2>
                                        <a href='/' className='Collection-shop mx-3 my-3'>Shop now</a>
                                    </div>
                                </div>
                            </Col>
                            <Col lg={5} >
                                <div className='collection-ultraboost banner__item d-flex justify-content-center'>
                                    <img src={require("../../assets/images/collection2022/ultraboost.jpg")}  className="collection-images" alt="ultaboost" />
                                    <div className='banner-collection-ultraboost' style={{float: "right"}}>
                                        <h2  className='mx-3 mt-2 mb-4'>Ultraboost Collection 2022</h2>
                                        <a href='/' className='Collection-shop mx-3 my-4'>Shop now</a>
                                    </div>
                                </div>
                            </Col>
                            <Col lg={7}>
                            <div className='banner__item StanSmith-collection'>
                                    <img src={require("../../assets/images/collection2022/StanSmith.png")}  className=" StanSmith-images" alt="ultaboost" />
                                    <div className='banner-collection-stansmith' style={{float: "right"}}>
                                        <h2  className='mx-3 mt-2 mb-4'>Stan Smith Are Back 2022</h2>
                                        <a href='/' className='Collection-shop mx-3 my-4'>Shop now</a>
                                    </div>
                                </div>
                            </Col>
                        </Row>
                    </Container>
                </div>
                {/* Products */}
                <div>
                    <Product></Product>
                </div>
                {/* Deal of the week */}
                <div>
                    <Container fluid className='Weekly-deal'>
                        <Row>
                            <Col lg={6}>
                                <img className='deal-image' alt={"deal"} src={require("../../assets/images/Deal-Shoes.png")}/>
                            </Col>
                            <Col lg={6} className=" text-center Deal-background">
                                <p className='text-light'>DEAL OF THE WEEK</p>
                                <h5 className='text-light'>ULTRA BOOST came back with the biggest deal of the month</h5>
                                <div className='clock'>
                                    <p className='text-light'>{timeDays}</p>
                                    <p className='ms-1'>: </p> 
                                
                                    <p className='text-light ms-1'>{timeHours}</p>
                                    <p className='ms-1'>: </p> 
                                
                                    <p className='text-light ms-1'>{timeMinutes}</p>
                                    <p className='ms-1'>: </p>
                                
                                    <p className='text-light ms-1'>{timeSeconds}</p>
                                </div>
                                <Button className='button-deal'>Shop Now</Button>
                            </Col>
                        </Row>
                    </Container>
                </div>
                <div>
                    <Container>
                        <Row className='advertisement'>
                            <Col sm={12} style={{display: "flex", justifyContent:"center", marginBottom:"30px"}}>
                                <p style={{fontSize:"30px"}}>KEEP MOVING</p>
                            </Col>
                            <Col sm={7}>
                                <div className='advertise-image'>
                                        <img src={require('../../assets/images/advertisement/1.jpg')} alt="advertise" className='item'/> 
                                        <img src={require('../../assets/images/advertisement/2.jpg')} alt="advertise" className='item'/> 
                                        <img src={require('../../assets/images/advertisement/3.jpg')} alt="advertise" className='item'/> 
                                        <img src={require('../../assets/images/advertisement/4.jpg')} alt="advertise" className='item'/> 
                                        <img src={require('../../assets/images/advertisement/5.jpg')} alt="advertise" className='item'/> 
                                        <img src={require('../../assets/images/advertisement/6.jpg')} alt="advertise" className='item'/> 
                                </div> 
                            </Col>
                            <Col sm={5} className="Instagram">
                                <h2>Instagram</h2>
                                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Numquam culpa ducimus autem ipsum voluptate magnam nisi, sequi eligendi consequuntur assumenda quibusdam temporibus fugit corrupti aut at quas nam, eum consequatur.</p>
                            </Col>
                        </Row>
                    </Container>
                </div>
                {/* History */}
                <div>
                    <Container fluid className='Background-history'>
                        <Container>
                            <Row>
                                <Col lg={6} className="my-5">
                                    <h1>
                                    STORIES, STYLES AND SPORTSWEAR AT ADIDAS, SINCE 1949 
                                    </h1>
                                    <p>
                                    Sport keeps us fit. Keeps you mindful. Brings us together. Through sport we have the power to change lives. Whether it is through stories of inspiring athletes. Helping you to get up and get moving. Sportswear featuring the latest technologies, to up your performance. Beat your PB.adidas offers a home to the runner, the basketball player, the soccer kid, the fitness enthusiast. The weekend hiker that loves to escape the city. The yoga teacher that spreads the moves. The 3-Stripes are seen in the music scene. On stage, at festivals. Our sports clothing keeps you focused before that whistle blows. During the race. And at the finish lines. We’re here to supportcreators. Improve their game. Their lives. And change the world. 
                                    </p><br/>
                                    <p>
                                    adidas is about more than sportswear and workout clothes. We partner with the best in the industry to co-create. This way we offer our fans the sports apparel and style that match their athletic needs, while keeping sustainability in mind. We’re here to support creators. Improve their game. Create change. And we think about the impact we have on our world.
                                    </p>
                                </Col>
                                <Col lg={6} className="my-5">
                                    <h1>
                                    WORKOUT CLOTHES, FOR ANY SPORT
                                    </h1>
                                    <p>
                                    adidas designs for and with athletes of all kinds. Creators, who love to change the game. Challenge conventions. Break the rules and define new ones. Then break them again. We supply teams and individuals with athletic clothing pre-match. To stay focussed. We design sports apparel that get you to the finish line. To win the match. We support women, with bras and tights made for purpose. From low to high support. Maximum comfort. We design, innovate and itterate. Testing new technologies in action. On the pitch, the tracks, the court, the pool. Retro workout clothes inspire new streetwear essentials. Like NMD, Ozweego and our Firebird tracksuits. Classic sports models are brought back to life. Like Stan Smith. And Superstar. Now seen on the streets and the stages.
                                    </p><br/>
                                    <p>
                                    Through our collections we blur the borders between high fashion and high performance. Like our adidas by Stella McCartney athletic clothing collection – designed to look the part inside and outside of the gym. Or some of our adidas Originals lifestyle pieces, that can be worn as sportswear too. Our lives are constantly changing. Becoming more and more versatile. And adidas designs with that in mind.
                                    </p>
                                </Col>
                            </Row>
                        </Container>
                    </Container>
                </div>
        </>
    )
}