import RouteList from "../Routes";
import { useLocation } from "react-router-dom";
import { Breadcrumb} from 'reactstrap';
import { Breadcrumbs, Link, Typography } from "@mui/material";
import NavigateNextIcon from '@mui/icons-material/NavigateNext';
export default  function ShopBreadcrump  () {
    const currentLocation = useLocation().pathname;

    const getRouteName = (pathname, routes) => {
        const currentRoute = RouteList.find((route) => route.path === pathname)
        return currentRoute ? currentRoute.name : false
      };
    const getBreadcrumbs = (location) => {
        const breadcrumbs = []
        location.split('/').reduce((prev, curr, index, array) => {
          const currentPathname = `${prev}/${curr}`
          const routeName = getRouteName(currentPathname, RouteList)
          routeName &&
            breadcrumbs.push({
              pathname: currentPathname,
              name: routeName,
              active: index + 1 === array.length ? true : false,
            })
          return currentPathname
        })
        return breadcrumbs
    }
    const breadcrumbs = getBreadcrumbs(currentLocation)
    return(
        <Breadcrumbs separator={<NavigateNextIcon fontSize="small" style={{color: "white"}} />} style={{position:"fixed", top:"120px", zIndex:"+1"}}>
            <Link underline="hover" href={"/"} sx={{color: "white"}}>Home</Link>
            {breadcrumbs.map((breadcrumb, index) => {
            return (
            <Link underline="hover" sx={{color: "white"}}
                {...(breadcrumb.active ? { underline: 'none' } : { href: breadcrumb.pathname })}
                key={index}
            >
                {breadcrumb.name}
            </Link>
            )
        })}
        </Breadcrumbs>
    )
}