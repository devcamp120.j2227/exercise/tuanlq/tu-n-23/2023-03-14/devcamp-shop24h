import { 
    SEND_REQUEST_TAKE_PRODUCT_PENDING,
    SEND_REQUEST_TAKE_PRODUCT_START,
    SEND_REQUEST_TAKE_PRODUCT_END

} from "../constant/constant";

export const fetchDataProduct = () => {
    return async (dispatch) => {
        try{

            dispatch({
                type: SEND_REQUEST_TAKE_PRODUCT_PENDING
            })
            var requestOption = {
                method:"GET",
            }
            const responseURL = await fetch ("/product", requestOption);

            const data = await responseURL.json();

            await dispatch({
                type: SEND_REQUEST_TAKE_PRODUCT_START,
                data: data
            })
        }
        catch (error){
            return({
                type: SEND_REQUEST_TAKE_PRODUCT_END,
                error: error
            })
        }
    }
}