import { Col, Row, Input } from 'reactstrap';
import { useState } from 'react';
export default function Footer () {
    
    return(  
        <footer className="bg-gray-dark">
            <Row className='d-flex justify-content-center'>
                <Col lg = {4} className="ms-2" >
                    <img src={require("../assets/images/adidas.png")} style={{height:"100px", marginTop: "20px"}} alt="adidas" />
                    <h5 >Contacts</h5>
                    <dl>
                    <dt style={{fontWeight:"bold"}}>Address:</dt>
                    <dd> ThuDuc District, SaiGon, VietNam</dd>
                    </dl>
                    <dl>
                    <dt style={{fontWeight:"bold"}}>Email:</dt>
                    <dd>
                        <a href="/" className="a-footer">info@example.com</a>
                    </dd>
                    </dl>
                    <dl>
                    <dt style={{fontWeight:"bold"}}>phones:</dt>
                    <dd>
                        <a href="/" className="a-footer">+91 99999999 </a><span>or</span> <a href="/" className="a-footer">+91 11111111</a>
                    </dd>
                    </dl>
                </Col>
                <Col lg = {5} className="mt-5 pt-2">
                    <Row>
                        <Col lg={6}>
                            <h3>More About Us</h3>
                            <ul className="nav-list">
                            <li>
                                <a className="a-footer links" id="links-about" href="/">
                                    About
                                </a>
                            </li>
                            </ul>
                            <ul className="nav-list">
                                <li>
                                    <a className="a-footer links" id="links-project" href="/">Projects</a>
                                </li>
                            </ul>
                            <ul className="nav-list">
                                <li>
                                    <a className="a-footer links" id="links-blog" href="/">Blog</a>
                                </li>
                            </ul>
                            <ul className="nav-list">
                                <li>
                                    <a className="a-footer links" id="links-contact" href="/">Contacts</a>
                                </li>
                            </ul>
                            <ul className="nav-list">
                                <li>
                                    <a className="a-footer links" id="links-pricing" href="/">Pricing</a>
                                </li>
                            </ul>
                        </Col>
                        <Col lg={6}>
                            <ul className='nav-list'>
                                <li>
                                    <h3>New Letter</h3>
                                </li>
                            </ul>
                            <ul className='nav-list'>
                                <li>
                                    <p>Be the first to know about new arival, look books, sales & promos!</p>
                                </li>
                                <Input className='Form-control' placeholder='@ your email ....'/>
                            </ul>
                        </Col>
                        </Row>
                </Col>
            </Row>
            <Row>
                <Col className="d-flex align-items-center justify-content-center">
                    <p>Devcamp IronHack Copyright © 2022 All rights reserved</p> 
                </Col>
            </Row>
        </footer>
    )
}